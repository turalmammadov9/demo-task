package turalmammadov9.demotask.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import turalmammadov9.demotask.exception.BadRequestException;
import turalmammadov9.demotask.model.Country;
import turalmammadov9.demotask.response.SuccessResponse;
import turalmammadov9.demotask.service.CountryService;

import javax.validation.Valid;
import java.text.MessageFormat;
import java.util.List;

@RestController
@RequestMapping("public/countries")
@AllArgsConstructor
public class CountryController {

        private final CountryService countryService;


    @PostMapping()
    public ResponseEntity<SuccessResponse> create(@RequestBody @Valid Country country) {
        if (!ObjectUtils.isEmpty(country.getId())) {
            throw new BadRequestException("A new data cannot already have an ID");
        }

        return new ResponseEntity<>(
                new SuccessResponse(countryService.create(country), "Successful registration"),
                HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<SuccessResponse> getAll() {
        List<Country> countries = countryService.getAll();

        return new ResponseEntity<>(new SuccessResponse(countries, MessageFormat.format("{0} Results found", countries.size())), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SuccessResponse> getOne(@PathVariable("id") Long id) {
        Country country = countryService.getOne(id);
        return new ResponseEntity<>(
                new SuccessResponse(country, "Result found"), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SuccessResponse> delete(@PathVariable("id") Long id) {
        countryService.deleteById(id);
        return new ResponseEntity<>(
                new SuccessResponse(null, "Deletion completed successfully"), HttpStatus.OK);
    }


}
