package turalmammadov9.demotask.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import turalmammadov9.demotask.model.LoginRequest;
import turalmammadov9.demotask.service.LoginService;

@RestController
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;


    @PostMapping("/public/login")
    public ResponseEntity login(@RequestBody LoginRequest request){
        HttpHeaders httpHeaders =  new HttpHeaders();
        loginService.login(request, httpHeaders);
        return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    }
}
