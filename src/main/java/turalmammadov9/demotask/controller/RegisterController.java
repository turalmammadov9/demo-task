package turalmammadov9.demotask.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import turalmammadov9.demotask.model.UserRegisterRequest;
import turalmammadov9.demotask.service.RegisterService;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE , makeFinal = true)
public class RegisterController {

    RegisterService registerService;

    @PostMapping("/public/register")
    public ResponseEntity<Object> registerUser(@RequestBody UserRegisterRequest request){
        registerService.registerUser(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
