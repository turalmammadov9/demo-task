package turalmammadov9.demotask;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication
public class DemoTaskApplication  {


    public static void main(String[] args) {
        SpringApplication.run(DemoTaskApplication.class, args);
    }

    }

