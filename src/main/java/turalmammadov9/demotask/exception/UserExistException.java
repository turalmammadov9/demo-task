package turalmammadov9.demotask.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserExistException extends RuntimeException{
    private String username;
}
