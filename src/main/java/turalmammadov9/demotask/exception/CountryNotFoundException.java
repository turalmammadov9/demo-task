package turalmammadov9.demotask.exception;

public class CountryNotFoundException extends RuntimeException{
    public CountryNotFoundException(String msg) {
        super(msg);
    }
}
