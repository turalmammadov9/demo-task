package turalmammadov9.demotask.exception;

public class CountryAlreadyExistsException extends RuntimeException {
    public CountryAlreadyExistsException(String msg) {
        super(msg);
    }
}
