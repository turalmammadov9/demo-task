package turalmammadov9.demotask.repo;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import turalmammadov9.demotask.model.Country;
import java.util.List;
import java.util.Optional;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface CountryMapper {

    String SELECT_FROM_COUNTRY_WHERE_ID = "SELECT * FROM country WHERE id = #{id}";
    String SELECT_FROM_COUNTRY = "select * from country";

    @Select(SELECT_FROM_COUNTRY)
    List<Country> findAll();

    @Select(SELECT_FROM_COUNTRY_WHERE_ID)
    Country findById(Long id);

    @Select("SELECT * FROM country WHERE name = #{name}")
    Country findByName(String name);

    @Delete("DELETE FROM country WHERE id = #{id}")
    boolean deleteById(Long id);

    @Insert("INSERT INTO country(name, capitalName, shortName, code) " +
            " VALUES (#{name}, #{capitalName}, #{shortName}, #{code})")
    void insert(Country country);

    @Update("Update country set name=#{name}, " +
            " capitalName=#{capitalName}, shortName=#{shortName}, code=#{code}, where id=#{id}")
    int update(Country country);
}
