package turalmammadov9.demotask.repo;

import turalmammadov9.demotask.model.User;
import turalmammadov9.demotask.model.UserRegisterRequest;
import java.util.Optional;

@org.apache.ibatis.annotations.Mapper
public interface UserMapper {


    Optional<User> findByUsername(String username);

    Optional<User> findUserById(Integer id);

    void insertUser(UserRegisterRequest request);


}
