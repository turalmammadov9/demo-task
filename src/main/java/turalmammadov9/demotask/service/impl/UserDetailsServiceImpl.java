package turalmammadov9.demotask.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import turalmammadov9.demotask.model.User;
import turalmammadov9.demotask.repo.UserMapper;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userMapper.findByUsername(username).orElseThrow(() -> new BadCredentialsException(""));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),true,
                user.isActive(),true,true, Collections.EMPTY_LIST);
    }
}
