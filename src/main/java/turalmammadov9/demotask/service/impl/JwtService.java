package turalmammadov9.demotask.service.impl;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import turalmammadov9.demotask.util.HttpUtil;

import java.util.Date;
import java.util.function.Function;

@Service
public class JwtService {

    private HttpUtil httpUtil;
    private static String secretKEy = "secretKey000000000000000000000000000000000000000000000000000000000000000000";
    private static Long exp = 1000000000L;

    public JwtService(HttpUtil httpUtil) {
        this.httpUtil = httpUtil;
    }


    public String generateToken(Authentication authentication){
        User user = (User) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject((user.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + exp))
                .signWith(SignatureAlgorithm.HS256, secretKEy)
                .compact();
    }


    public String getPinFromToken() {
        return getClaimFromToken(httpUtil.getTokenFromHeader(), Claims::getSubject);
    }



    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        Claims claims = Jwts.parser().setSigningKey(secretKEy).parseClaimsJws(token).getBody();
        return claimsResolver.apply(claims);
    }

    public boolean validateToken(String token) {
        try {
            if (!isTokenExpired(token)) {
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public boolean isTokenExpired(String token) {
        return getClaimFromToken(token, Claims::getExpiration).before(new Date(System.currentTimeMillis()));
    }


}

