package turalmammadov9.demotask.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import turalmammadov9.demotask.exception.CountryAlreadyExistsException;
import turalmammadov9.demotask.exception.CountryNotFoundException;
import turalmammadov9.demotask.model.Country;
import turalmammadov9.demotask.repo.CountryMapper;
import turalmammadov9.demotask.service.CountryService;

import java.text.MessageFormat;
import java.util.List;


@Service
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService {

    private final CountryMapper countryMapper;

    @Override
    public Country create(Country country) {
        Country countryById = getByName(country.getName());
        if(!ObjectUtils.isEmpty(countryById)){
            throw new CountryAlreadyExistsException(MessageFormat.format("Country {0} already exists in the system", country.getName()));
        }
        countryMapper.insert(country);
        return getByName(country.getName());
    }

    public List<Country> getAll() {

            return countryMapper.findAll ();
    }

    @Override
    public Country getOne(long id) {
        Country country = countryMapper.findById(id);
        if(ObjectUtils.isEmpty(country)){
            throw new CountryNotFoundException(MessageFormat.format("Country id {0} not found", String.valueOf(id)));
        }
        return country;
    }

    @Override
    public void deleteById(long id) {
         countryMapper.deleteById(id);
    }

    @Override
    public Country getByName(String name) {
        return countryMapper.findByName(name);
    }
}
