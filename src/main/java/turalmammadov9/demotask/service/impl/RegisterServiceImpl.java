package turalmammadov9.demotask.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import turalmammadov9.demotask.exception.UserExistException;
import turalmammadov9.demotask.model.UserRegisterRequest;
import turalmammadov9.demotask.repo.UserMapper;
import turalmammadov9.demotask.service.RegisterService;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegisterServiceImpl implements RegisterService {

    private final BCryptPasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    @Override
    public void registerUser(UserRegisterRequest request) {
        if (validateIfUserExist(request.getUsername())) {
            throw new UserExistException(request.getUsername());
        }
        request.setPassword( passwordEncoder.encode(request.getPassword()));
        userMapper.insertUser(request);
        log.info("User with username - "  + request.getUsername() + " registered successfully" );
    }



    public boolean validateIfUserExist(String username) throws UserExistException {
        return userMapper.findByUsername(username).isPresent();
    }
}
