package turalmammadov9.demotask.service;


import org.springframework.http.HttpHeaders;
import turalmammadov9.demotask.model.LoginRequest;

public interface LoginService {
    void login(LoginRequest request, HttpHeaders httpHeaders);

}
