package turalmammadov9.demotask.service;


import turalmammadov9.demotask.model.UserRegisterRequest;

public interface RegisterService {

    void registerUser(UserRegisterRequest request);
}
