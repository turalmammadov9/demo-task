package turalmammadov9.demotask.service;

import org.springframework.stereotype.Service;
import turalmammadov9.demotask.model.Country;

import java.util.List;

@Service
public interface CountryService {
    Country create(Country country);

    List<Country> getAll();

    Country getOne(long id);

    void deleteById(long id);

    Country getByName(String name);
}
