package turalmammadov9.demotask.model;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserRegisterRequest {

    @NonNull
    private Integer id;
    private String username;
    @ToString.Exclude
    @NonNull
    private String password;
    private String email;
}
