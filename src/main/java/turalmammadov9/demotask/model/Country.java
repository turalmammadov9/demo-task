package turalmammadov9.demotask.model;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Country {

    private Long id;
    private String name;
    private String capitalName;
    private String shortname;
    private Integer code;


}
