package turalmammadov9.demotask.model;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class User  {

    Integer id;
    String username;
    String password;
    String email;
    private boolean isActive;




}
