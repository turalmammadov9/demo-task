package turalmammadov9.demotask.response;

import lombok.*;

import java.time.Instant;

public class ErrorResponse {

    private String code;
    /**
     * short error message
     */
    private String message;

    /**
     * error cause timestamp
     */
    private Instant timestamp;
}
