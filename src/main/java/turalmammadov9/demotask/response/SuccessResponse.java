package turalmammadov9.demotask.response;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuccessResponse {

    private Object data;
    private String message;
}
